const express = require('express')
const session = require('express-session')
const app = express();
const bodyParser = require('body-parser')
const config = require('./config/config_db')
const cors = require('cors')


const option = {
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}
app.use(cors())
app.options('*', cors())
app.use(session(option))
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use((req, res, next) => {
    req.db = require('./connect/db')
    req.db_mqtt = require('./connect/db_mqtt')
    next()
})
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST')
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next();
})

app.use('/api', require('./api'))
app.use('/manage', require('./manage'))

app.listen(config.port, () => {
    console.log('ready', config.port)
})