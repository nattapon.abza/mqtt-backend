const express = require('express')
const router = express.Router()
const authen = require('../config/authen')


module.exports = router

router.get('/', authen, async(req, res) => {
    let db = req.db
    if (req.user_id != undefined) {
        let rows = await db('tbl_company_mqtt').select('company_id', 'company_name').where({ user_id: req.user_id })
        if (rows != '') {
            let company_id = rows[0].company_id
            let apps = await db('tbl_app_mqtt').select('app_id', 'app_name').where({ company_id: company_id, active: 1 })
            if (apps != '') {
                res.send(apps)
            } else {
                res.send({ message: 'no_data' })
            }
        } else {
            res.send({ message: 'no_data' })
        }
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/create', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let rows = await db('tbl_company_mqtt').select('company_id', 'company_name').where({ user_id: req.body.user_id })
        if (rows != '') {
            let company_id = rows[0].company_id
            let apps = await db('tbl_app_mqtt').select('app_id', 'app_name').where({ company_id: company_id, active: 1, app_name: (req.body.app_name).toUpperCase() })
            if (apps == '') {
                await db('tbl_app_mqtt').insert({ company_id: company_id, app_name: (req.body.app_name).toUpperCase() })
                res.send({ message: 'success' })
            } else {
                res.send({ message: 'cant_create' })
            }

        } else {
            res.send({ message: 'no_company' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})

router.post('/edit', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let apps = await db('tbl_app_mqtt').select('app_id', 'app_name').where({ app_id: req.body.app_id, active: 1 })
        if (apps != '') {
            await db('tbl_app_mqtt').where({ app_id: req.body.app_id }).update({ app_name: (req.body.app_name).toUpperCase() })
            res.send({ message: 'success' })
        } else {
            res.send({ message: 'no_app' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/del', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let apps = await db('tbl_app_mqtt').select('app_id', 'app_name').where({ app_id: req.body.app_id, active: 1 })
        if (apps != '') {
            await db('tbl_app_mqtt').where({ app_id: req.body.app_id }).update({ active: 0 })
            res.send({ message: 'success' })
        } else {
            res.send({ message: 'no_app' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})