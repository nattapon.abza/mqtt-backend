const express = require('express')
const router = express.Router()

module.exports = router
var sess

router.use('/register', require('./register'))
router.use('/login', require('./login'))
router.use('/logout', require('./logout'))
router.use('/users', require('./users'))
router.use('/gentoken', require('./gentoken'))
router.use('/profile', require('./profile'))
router.use('/changepass', require('./changepass'))
router.use('/company', require('./company'))
router.use('/app', require('./app'))
router.use('/hotel', require('./hotel'))
router.use('/buiding', require('./buiding'))
router.use('/floor', require('./floor'))
router.use('/room', require('./room'))
router.use('/topic', require('./topic'))
router.use('/checkuser', require('./checkuser'))
router.use('/alltopic', require('./alltopic'))