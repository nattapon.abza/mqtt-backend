const express = require('express')
const router = express.Router()
const authen = require('../config/authen')


module.exports = router

router.get('/:user_id', authen, async(req, res) => {
    let db = req.db
    if (req.user_id != undefined) {
        let rows = await db('tbl_company_mqtt').select('company_id', 'company_name').where({ user_id: req.params.user_id })
        if (rows != '') {
            res.send({
                company_id: rows[0].company_id,
                company_name: rows[0].company_name
            })
        } else {
            res.send({ message: 'no_data' })
        }
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/create', authen, async(req, res) => {
    let db = req.db
    let db_mqtt = req.db_mqtt
    if (req.user_id != undefined) {
        let rows = await db('tbl_company_mqtt').select('company_id', 'company_name').where({ user_id: req.body.user_id })
        let acl = await db_mqtt('mqtt_acl').select('acl_id', 'topic').where({ user_id: req.body.user_id })
        if (rows != '') {
            res.send({ message: 'cant_create' })
        } else {
            let rows_com = await db('tbl_company_mqtt').select('company_id', 'company_name').where({ company_name: req.body.company_name })
            if (req.body.company_name == undefined || rows_com != '') {
                res.send({ message: 'fail' })
            } else {
                await db('tbl_company_mqtt').insert({ user_id: req.body.user_id, company_name: (req.body.company_name).toUpperCase() })
                for (var i = 0; i < acl.length; i++) {
                    await db_mqtt('mqtt_acl').where({ user_id: req.body.user_id, acl_id: acl[i].acl_id }).update({ topic: genTopic(acl[i].topic, (req.body.company_name).toUpperCase()) })
                    console.log(i)
                }
                res.send({ message: 'success' })
            }
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})

function genTopic(topic, company) {
    var topics = topic.split('/')
    return company + '/' + topics[1] + '/' + topics[2] + '/' + topics[3] + '/' + topics[4] + '/' + topics[5] + '/' + topics[6] + '/' + topics[7]
}