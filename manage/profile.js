const express = require('express')
const router = express.Router()
const authen = require('../config/authen')

module.exports = router

router.get('/', authen, async(req, res) => {
    let db = req.db_mqtt
    if (req.user_id != '') {
        let user_id = req.user_id
        let rows = await db('mqtt_users').where({ user_id: user_id })
        let user = {
            user_id: user_id,
            username: rows[0].username,
            firstname: rows[0].firstname,
            lastname: rows[0].lastname,
            email: rows[0].email,
            organization_name: rows[0].organization_name
        }
        res.send(user)
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }

})

router.post('/edit', authen, async(req, res) => {
    let db = req.db_mqtt
    let user_id = req.body.user_id
    let data_update = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        organization_name: req.body.organization_name
    }
    if (req.user_id != '') {
        let rows = await db('mqtt_users').where({ user_id: user_id }).update(data_update)
        res.send({ message: 'success' })
    } else {
        return res.status(401).json({
            "status": 401,
            "message": "Unauthorized"
        })
    }

})