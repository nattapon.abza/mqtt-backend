const express = require('express')
const router = express.Router()
const authen = require('../config/authen')


module.exports = router

router.get('/:user_id/:app_id/:hotel_id/:buiding', authen, async(req, res) => {
    let db = req.db
    if (req.params.user_id != undefined) {
        let floor = await db('tbl_all_mqtt').select('floor')
            .groupBy('floor')
            .where({
                app_id: req.params.app_id,
                hotel_id: req.params.hotel_id,
                buiding: req.params.buiding,
                active: 1
            })
        if (floor != '') {
            res.send(floor)
        } else {
            res.send({ message: 'no_data' })
        }
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/create', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let floor = await db('tbl_all_mqtt').select('floor')
            .where({
                app_id: req.body.app_id,
                hotel_id: req.body.hotel_id,
                buiding: req.body.buiding.toUpperCase(),
                floor: req.body.floor.toUpperCase(),
                active: 1
            })
        if (floor == '') {
            await db('tbl_all_mqtt')
                .insert({
                    app_id: req.body.app_id,
                    hotel_id: req.body.hotel_id,
                    buiding: req.body.buiding.toUpperCase(),
                    floor: req.body.floor.toUpperCase(),
                    room: 'ROOMDEMO'
                })
            res.send({ message: 'success' })
        } else {
            res.send({ message: 'cant_create' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})

router.post('/edit', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let floor = await db('tbl_all_mqtt').select('floor')
            .where({
                app_id: req.body.app_id,
                hotel_id: req.body.hotel_id,
                buiding: req.body.buiding.toUpperCase(),
                floor: req.body.floor.toUpperCase(),
                active: 1
            })
            .where('floor', '!=', req.body.floor_old.toUpperCase())
        if (floor == '') {
            await db('tbl_all_mqtt').where({ floor: req.body.floor_old.toUpperCase() }).update({ floor: req.body.floor.toUpperCase() })
            res.send({ message: 'success' })
        } else {
            res.send({ message: 'cant_edit' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/del', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        await db('tbl_all_mqtt').where({
            app_id: req.body.app_id,
            hotel_id: req.body.hotel_id,
            buiding: req.body.buiding.toUpperCase(),
            floor: req.body.floor
        }).update({ active: 0 })
        res.send({ message: 'success' })
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})