const express = require('express')
const router = express.Router()
const authen = require('../config/authen')

module.exports = router

router.get('/', authen, async(req, res) => {
    let db = req.db_mqtt
    if (req.user_id != '') {
        let user_id = req.user_id
        let rows = await db('mqtt_users').where({ user_id: user_id })
        let user = {
            user_id: user_id,
            username: rows[0].username,
            firstname: rows[0].firstname,
            lastname: rows[0].lastname,
            email: rows[0].email,
            organization_name: rows[0].organization_name,
            user_roll: rows[0].user_roll
        }
        res.send(user)
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }

})