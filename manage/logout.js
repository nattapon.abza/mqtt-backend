const express = require('express')
const router = express.Router()

module.exports = router

router.post('/', async(req, res) => {
    req.session.destroy();
    res.send({ message: 'success' })
})