const express = require('express')
const router = express.Router()
const crypto = require('crypto')
const authen = require('../config/authen')
module.exports = router

router.post('/', authen, async(req, res) => {
    let hash = crypto.createHash('sha256')
    let db = req.db_mqtt
    let user_id = req.user_id
    if (req.user_id != '') {
        let password = hash.update(req.body.password).digest('hex')
        await db('mqtt_users').where({ user_id: user_id }).update({ password: password })

        res.send({ message: 'success' })
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }

})