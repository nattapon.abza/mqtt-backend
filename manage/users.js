const express = require('express')
const router = express.Router()
const crypto = require('crypto')
const generator = require('generate-password')
const moment = require('moment')
const sendmail = require('../connect/sendmail')
const authen = require('../config/authen')

module.exports = router

const topics = [{ access: 3, topic: '+/+/+/+/+/+/+/CMD' },
    { access: 1, topic: '+/+/+/+/+/+/+/RESPONSE' },
    { access: 1, topic: '+/+/+/+/+/+/+/STATUS' }
]

router.get('/', authen, async(req, res) => {
    let db = req.db_mqtt
    if (req.user_roll === 1) {
        let rows = await db('mqtt_users')
        if (rows != '') {
            res.send({ users: rows })
                //res.send({ user_roll: req.user_roll })
        } else {
            res.send({ message: 'not_user' })
        }
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }
})

router.get('/profile/:user_id', authen, async(req, res) => {
    let db = req.db_mqtt
    if (req.user_roll === 1) {
        let user_id = req.params.user_id
        console.log(user_id)
        let rows = await db('mqtt_users').where({ user_id: user_id })
        let user = {
            user_id: user_id,
            username: rows[0].username,
            firstname: rows[0].firstname,
            lastname: rows[0].lastname,
            email: rows[0].email,
            organization_name: rows[0].organization_name
        }
        res.send(user)
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }

})
router.post('/edit/:user_id', authen, async(req, res) => {
    let db = req.db_mqtt
    let user_id = req.params.user_id
    let data_update = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        organization_name: req.body.organization_name
    }
    if (req.user_roll === 1) {
        let rows = await db('mqtt_users').where({ user_id: user_id }).update(data_update)
        res.send({ message: 'success' })
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }

})
router.post('/changepass/:user_id', authen, async(req, res) => {
    let hash = crypto.createHash('sha256')
    let db = req.db_mqtt
    let user_id = req.params.user_id
    if (req.user_roll === 1) {
        let password = hash.update(req.body.password).digest('hex')
        await db('mqtt_users').where({ user_id: user_id }).update({ password: password })

        res.send({ message: 'success' })
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }

})
router.post('/create', authen, async(req, res) => {
    let hash = crypto.createHash('sha256')
    let date_now = moment().format('YYYY-MM-DD HH:mm:ss')
    let db = req.db_mqtt
    var password = generator.generate({
        length: 10,
        numbers: true,
        symbols: true
    })

    let password_hash = hash.update(password).digest('hex')
    console.log(password_hash)
    let user = {
        username: req.body.username,
        firstname: req.body.firstname,
        password: password_hash,
        lastname: req.body.lastname,
        email: req.body.email,
        organization_name: req.body.organization_name,
        create_date: date_now
    }
    if (req.user_roll === 1) {
        let rows = await db('mqtt_users').where({ username: req.body.username })
        if (rows != '') {
            res.send({ message: 'cannot_username' })
        } else {
            let row_insert = await db('mqtt_users').insert(user)
            for (let i = 0; i < topics.length; i++) {
                await db('mqtt_acl').insert({
                    user_id: row_insert,
                    allow: 1,
                    username: req.body.username,
                    access: topics[i].access,
                    topic: topics[i].topic
                })
            }
            //console.log(row_insert)
            res.send({ message: 'success', username: req.body.username, password: password })
            await sendmail(req.body.email, req.body.username, password)
        }
    } else {
        return res.status(401).json({
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/disable', authen, async(req, res) => {
    let db = req.db_mqtt
    let active = req.body.active
    let user_id = req.body.user_id
    if (req.user_roll === 1) {
        await db('mqtt_users').where({ user_id: user_id }).update({ active: active })
        res.send({ message: 'success' })
    } else {
        return res.status(401).json({
            "status": 401,
            "message": "Unauthorized"
        })
    }
})