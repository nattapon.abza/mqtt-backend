const express = require('express')
const router = express.Router()
const authen = require('../config/authen')


module.exports = router

router.get('/:user_id/:app_id', authen, async(req, res) => {
    let db = req.db
    console.log(req.body)
    if (req.params.user_id != undefined) {
        let hotel = await db('tbl_hotel_mqtt').select('hotel_id', 'hotel_name').where({ app_id: req.params.app_id, active: 1 })
        if (hotel != '') {
            res.send(hotel)
        } else {
            res.send({ message: 'no_data' })
        }
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/create', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let hotel = await db('tbl_hotel_mqtt').select('hotel_id', 'hotel_name').where({ app_id: req.body.app_id, active: 1, hotel_name: req.body.hotel_name.toUpperCase() })
        if (hotel == '') {
            await db('tbl_hotel_mqtt').insert({ app_id: req.body.app_id, hotel_name: req.body.hotel_name.toUpperCase() })
            res.send({ message: 'success' })

        } else {
            res.send({ message: 'cant_create' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})

router.post('/edit', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let hotel = await db('tbl_hotel_mqtt').select('hotel_id', 'hotel_name').where({ app_id: req.body.app_id, active: 1, hotel_id: req.body.hotel_id })
        if (hotel != '') {
            await db('tbl_hotel_mqtt').where({ hotel_id: req.body.hotel_id }).update({ hotel_name: (req.body.hotel_name).toUpperCase() })
            res.send({ message: 'success' })
        } else {
            res.send({ message: 'no_hotel' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/del', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let hotel = await db('tbl_hotel_mqtt').select('hotel_id', 'hotel_name').where({ app_id: req.body.app_id, active: 1, hotel_id: req.body.hotel_id })
        if (hotel != '') {
            await db('tbl_hotel_mqtt').where({ hotel_id: req.body.hotel_id }).update({ active: 0 })
            res.send({ message: 'success' })
        } else {
            res.send({ message: 'no_hotel' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})