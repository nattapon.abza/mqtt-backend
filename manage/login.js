const express = require('express')
const router = express.Router()
const crypto = require('crypto')
const jwt = require('jsonwebtoken') // ใช้งาน jwt module
const fs = require('fs')

module.exports = router

router.post('/', async(req, res) => {
    //const privateKey = fs.readFileSync(__dirname + '/../config/private.key')
    let hash = crypto.createHash('sha256')
    let sess = req.session
    let db = req.db_mqtt
    let password = hash.update(req.body.password).digest('hex')
    let rows = await db('mqtt_users').where({ username: req.body.username, password: password, active: 1 })
    if (rows != '') {
        sess.user_id = rows[0].user_id
        sess.username = rows[0].username
        sess.firstname = rows[0].firstname
        sess.lastname = rows[0].lastname
        sess.email = rows[0].email
        sess.organization = rows[0].organization_name
        sess.user_roll = rows[0].user_roll

        const privateKey = fs.readFileSync(__dirname + '/../config/private.key')
        const payload = {
            id: rows[0].user_id,
            username: rows[0].username,
            organization: rows[0].organization_name
        }

        const token = jwt.sign(payload, privateKey)
        res.send({ token: token, user_id: rows[0].user_id, message: 'success' })
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }

})