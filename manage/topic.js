const express = require('express')
const router = express.Router()


module.exports = router

router.get('/', async(req, res) => {
    let db = req.db
    if (req.session.user_id != undefined) {
        let topic = await db('tbl_company_mqtt')
            .innerJoin('tbl_app_mqtt', 'tbl_app_mqtt.company_id', 'tbl_company_mqtt.company_id')
            .innerJoin('tbl_hotel_mqtt', 'tbl_hotel_mqtt.app_id', 'tbl_app_mqtt.app_id')
            .innerJoin('tbl_all_mqtt', 'tbl_all_mqtt.hotel_id', 'tbl_hotel_mqtt.hotel_id')
            .select('tbl_company_mqtt.company_name', 'tbl_app_mqtt.app_name', 'tbl_hotel_mqtt.hotel_name',
                'tbl_all_mqtt.buiding', 'tbl_all_mqtt.floor', 'tbl_all_mqtt.room')
            .where('tbl_company_mqtt.user_id', '=', req.session.user_id)
        if (topic != '') {
            res.send(topic)
        } else {
            res.send({ message: 'no_data' })
        }
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }
})