const express = require('express')
const router = express.Router()
const crypto = require('crypto')

const moment = require('moment')
const generator = require('generate-password')
const sendmail = require('../connect/sendmail')

module.exports = router

const topics = [{ access: 3, topic: '+/+/+/+/+/+/+/CMD' },
    { access: 1, topic: '+/+/+/+/+/+/+/RESPONSE' },
    { access: 1, topic: '+/+/+/+/+/+/+/STATUS' }
]
router.post('/', async(req, res) => {
    let hash = crypto.createHash('sha256')
    let date_now = moment().format('YYYY-MM-DD HH:mm:ss')
    let db = req.db_mqtt
    var password = generator.generate({
        length: 10,
        numbers: true,
        symbols: true
    })

    let password_hash = hash.update(password).digest('hex')
    console.log(password_hash)
    let user = {
        username: req.body.username,
        firstname: req.body.firstname,
        password: password_hash,
        lastname: req.body.lastname,
        email: req.body.email,
        organization_name: req.body.organization_name,
        create_date: date_now
    }
    let rows = await db('mqtt_users').where({ username: req.body.username })
    if (rows != '') {
        res.send({ message: 'cannot_username' })
    } else {
        let row_insert = await db('mqtt_users').insert(user)
        for (let i = 0; i < topics.length; i++) {
            await db('mqtt_acl').insert({
                user_id: row_insert,
                allow: 1,
                username: req.body.username,
                access: topics[i].access,
                topic: topics[i].topic
            })
        }
        //console.log(row_insert)
        await sendmail(req.body.email, req.body.username, password)
        res.send({ username: req.body.username, password: password })
    }
})