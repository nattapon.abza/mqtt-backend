const express = require('express')
const router = express.Router()
const authen = require('../config/authen')

module.exports = router

router.get('/:user_id/:app_id/:hotel_id/:buiding/:floor', authen, async(req, res) => {
    let db = req.db
    if (req.params.user_id != undefined) {
        let room = await db('tbl_all_mqtt').select('all_id', 'room')
            .where({
                app_id: req.params.app_id,
                hotel_id: req.params.hotel_id,
                buiding: req.params.buiding,
                floor: req.params.floor,
                active: 1
            })
        if (room != '') {
            res.send(room)
        } else {
            res.send({ message: 'no_data' })
        }
    } else {
        return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/create', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let room = await db('tbl_all_mqtt').select('room')
            .where({
                app_id: req.body.app_id,
                hotel_id: req.body.hotel_id,
                room: req.body.room.toUpperCase(),
                active: 1
            })
        if (room == '') {
            await db('tbl_all_mqtt')
                .insert({
                    app_id: req.body.app_id,
                    hotel_id: req.body.hotel_id,
                    buiding: req.body.buiding.toUpperCase(),
                    floor: req.body.floor.toUpperCase(),
                    room: req.body.room.toUpperCase()
                })
            res.send({ message: 'success' })
        } else {
            res.send({ message: 'cant_create' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})

router.post('/edit', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        let room = await db('tbl_all_mqtt').select('room')
            .where({
                app_id: req.body.app_id,
                hotel_id: req.body.hotel_id,
                room: req.body.room.toUpperCase(),
                active: 1
            })
            .where('all_id', '!=', req.body.all_id)
        if (room == '') {
            await db('tbl_all_mqtt').where({ all_id: req.body.all_id }).update({ room: req.body.room.toUpperCase() })
            res.send({ message: 'success' })
        } else {
            res.send({ message: 'cant_room' })
        }
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/del', authen, async(req, res) => {
    let db = req.db
    if (req.body.user_id != undefined) {
        await db('tbl_all_mqtt').where({
            app_id: req.body.app_id,
            hotel_id: req.body.hotel_id,
            all_id: req.body.all_id
        }).update({ active: 0 })
        res.send({ message: 'success' })
    } else {
        return res.status(401).json({ // หากไมมี session
            "status": 401,
            "message": "Unauthorized"
        })
    }
})