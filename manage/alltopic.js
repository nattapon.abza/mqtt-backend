const express = require('express')
const router = express.Router()
const authen = require('../config/authen')


module.exports = router
router.get('/:user_id', authen, async(req, res) => {
    let db = req.db
    if (req.params.user_id != undefined) {
        let topicall = await db('tbl_company_mqtt')
            .innerJoin('tbl_app_mqtt', 'tbl_company_mqtt.company_id', 'tbl_app_mqtt.company_id')
            .innerJoin('tbl_hotel_mqtt', 'tbl_hotel_mqtt.app_id', 'tbl_app_mqtt.app_id')
            .innerJoin('tbl_all_mqtt', 'tbl_all_mqtt.hotel_id', 'tbl_hotel_mqtt.hotel_id')
            .innerJoin('tbl_electric_match', 'tbl_electric_match.all_id', 'tbl_all_mqtt.all_id')
            .select('tbl_electric_match.topic', 'tbl_electric_match.topic_id')
            .where('tbl_company_mqtt.user_id', '=', req.params.user_id)
            .where('tbl_all_mqtt.active', '=', 1)
        res.send(topicall)
    } else {
        return res.status(401).json({
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.get('/gettopic_id/:all_id', authen, async(req, res) => {
    let db = req.db
    let topic = await db('tbl_electric_match').select('topic_id', 'topic').where({ all_id: req.params.all_id })
    res.send({ topic_id: topic[0].topic_id })
})
router.get('/:user_id/update', authen, async(req, res) => {
    let db = req.db
    if (req.params.user_id != undefined) {
        let topicall = await db('tbl_company_mqtt')
            .innerJoin('tbl_app_mqtt', 'tbl_company_mqtt.company_id', 'tbl_app_mqtt.company_id')
            .innerJoin('tbl_hotel_mqtt', 'tbl_hotel_mqtt.app_id', 'tbl_app_mqtt.app_id')
            .innerJoin('tbl_all_mqtt', 'tbl_all_mqtt.hotel_id', 'tbl_hotel_mqtt.hotel_id')
            .select('tbl_company_mqtt.company_name', 'tbl_app_mqtt.app_name', 'tbl_hotel_mqtt.hotel_name',
                'tbl_all_mqtt.buiding', 'tbl_all_mqtt.floor', 'tbl_all_mqtt.room', 'tbl_all_mqtt.all_id')
            .where('tbl_company_mqtt.user_id', '=', req.params.user_id)
            .where('tbl_all_mqtt.active', '=', 1)
        for (var i = 0; i < topicall.length; i++) {
            let match = await db('tbl_electric_match').where({ all_id: topicall[i].all_id })
            if (match != '') {
                await db('tbl_electric_match').update({ topic: genTopic(topicall[i]) }).where({ all_id: topicall[i].all_id })
            } else {
                await db('tbl_electric_match').insert({ all_id: topicall[i].all_id, topic: genTopic(topicall[i]), command: 'OFF' })
            }
        }
        res.send({ message: 'success' })
    } else {
        return res.status(401).json({
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.post('/:user_id', authen, async(req, res) => {
    let db = req.db
    if (req.params.user_id != undefined) {
        let topicall = await db('tbl_company_mqtt')
            .innerJoin('tbl_app_mqtt', 'tbl_company_mqtt.company_id', 'tbl_app_mqtt.company_id')
            .innerJoin('tbl_hotel_mqtt', 'tbl_hotel_mqtt.app_id', 'tbl_app_mqtt.app_id')
            .innerJoin('tbl_all_mqtt', 'tbl_all_mqtt.hotel_id', 'tbl_hotel_mqtt.hotel_id')
            .innerJoin('tbl_electric_match', 'tbl_electric_match.all_id', 'tbl_all_mqtt.all_id')
            .select('tbl_electric_match.topic', 'tbl_electric_match.topic_id')
            .where('tbl_company_mqtt.user_id', '=', req.params.user_id)
            .where('tbl_all_mqtt.active', '=', 1)
            .where('topic', 'like', '%' + req.body.search + '%')

        res.send(topicall)
    } else {
        return res.status(401).json({
            "status": 401,
            "message": "Unauthorized"
        })
    }
})
router.get('/topic/:topic_id', authen, async(req, res) => {
    let db = req.db

    let topic = await db('tbl_electric_match').select('topic').where({ topic_id: req.params.topic_id })
    res.send({ topic: topic[0].topic })
})

function genTopic(topic) {
    return topic.company_name + '/' + topic.app_name + '/' + topic.hotel_name + '/' + topic.buiding + '/' + topic.floor + '/' + topic.room + '/POWER/'
}