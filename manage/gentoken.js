const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken') // ใช้งาน jwt module
const fs = require('fs')
const authen = require('../config/authen')

module.exports = router

router.get('/:user_id', authen, async(req, res) => {
    let db = req.db_mqtt
    const privateKey = fs.readFileSync(__dirname + '/../config/private.key')
    if (req.params.user_id !== undefined) {
        let rows = await db('mqtt_users').where({ user_id: req.params.user_id })
        const payload = {
            id: rows[0].user_id,
            username: rows[0].username,
            organization: rows[0].organization_name
        }

        const token = jwt.sign(payload, privateKey)
        res.send({ token: token })
    } else {
        return res.status(401).json({
            "status": 401,
            "message": "Unauthorized"
        })
    }
})