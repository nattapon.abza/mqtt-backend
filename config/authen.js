const jwt = require('jsonwebtoken') // ใช้งาน jwt module
const fs = require('fs')

const authorization = (async(req, res, next) => {
    const authorization = req.headers['authorization']
    const db = req.db_mqtt
    const sess = req.session
        // ถ้าไม่มีการส่งค่ามา ส่ง ข้อความ json พร้อม status 401 Unauthorized
    if (authorization === undefined) return res.status(401).json({
            "status": 401,
            "message": "Unauthorized"
        })
        // ถ้ามีการส่งค่ามา แยกเอาเฉพาะค่า token จากที่ส่งมา 'Bearer xxxx' เราเอาเฉพาะ xxxx
        // แยกข้อความด้วยช่องว่างได้ array สองค่า เอา array key ตัวที่สองคือ 1 
        // array key เริ่มต้นที่ 0 จะเได้ key เท่ากับ 1 คือค่า xxxx ที่เป้น token
    const token = req.headers['authorization'].split(' ')[1]
    if (token === undefined) return res.status(401).json({ // หากไมมีค่า token
            "status": 401,
            "message": "Unauthorized"
        })
        // ใช้ค่า privateKey เ็น buffer ค่าที่อ่านได้จากไฟล์ private.key ในโฟลเดอร์ config
    const privateKey = fs.readFileSync(__dirname + '/../config/private.key')
        // ทำการยืนยันความถูกต้องของ token
    jwt.verify(token, privateKey, async function(error, decoded) {
        if (error) return res.status(401).json({ // หาก error ไม่ผ่าน
                "status": 401,
                "message": "Unauthorized"
            })
            //console.log(error)
            //console.log(decoded)
            //console.log(rows)
        if (decoded.id === undefined || decoded.username === undefined || decoded.organization === undefined) {
            return res.status(403).json({
                "status": 403,
                "message": "Forbidden"
            })
        } else {
            sess.token_id = decoded.id
            req.user_id = decoded.id
            req.username = decoded.username
            req.organization_name = decoded.organization
            const rows = await db('mqtt_users').where({ user_id: decoded.id, username: decoded.username, organization_name: decoded.organization })
            req.user_roll = rows[0].user_roll
            if (rows === '') {
                return res.status(403).json({
                    "status": 403,
                    "message": "Forbidden"
                })
            }
        }
        // ถ้าทุกอย่างผ่าน ทุกเงื่อนไข ก็ไปทำ middleware ฟังก์ชั่นในลำดับถัดไป
        next()
    })
})

module.exports = authorization // ส่ง middleware ฟังก์ชั่นไปใช้งาน