const express = require('express')
const router = express.Router()

module.exports = router

router.use('/publish', require('./publish'))
router.use('/subscribe', require('./subscribe'))