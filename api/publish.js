const express = require('express')
const router = express.Router()
const mqtt = require('mqtt')
const authen = require('../config/authen')
module.exports = router

router.post('/', authen, async(req, res) => {
    let db = req.db
    let topic = req.body.topic
    let command = req.body.command
    if (topic === '' || command === '') {
        res.send({ message: 'fail' })
    } else {
        //console.log(topic.split('CMD').join(''))
        let rows = await db('tbl_electric_match').where('topic', '=', topic.split('CMD').join(''))
        if (rows != '') {
            console.log(rows)
            await db('tbl_electric_match').where('topic', '=', topic.split('CMD').join('')).update({ command: command })
            res.send({ message: 'success' })
        } else {
            res.send({ message: 'no_topic' })
        }
    }
    //res.send({ message: 'success' })


    /* var options = {
        clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
        username: "nelleh",
        password: "qazwsx",
        clean: true
    }
    console.log(req.body)
    var client = mqtt.connect('mqtt://staging.nellehliving.com', options)
    client.on('connect', function() {
            client.publish(req.body.topic, req.body.command, { retain: true, qos: 2 }, async function(error) {
                if (!error) {
                    res.send({ message: 'success' })

                } else {
                    res.send({ message: 'fail' })
                }
            })
            client.end()
        }) */
})