const express = require('express')
const router = express.Router()
const mqtt = require('mqtt')
const authen = require('../config/authen')

module.exports = router

router.post('/', authen, async(req, res) => {
    let db = req.db
    let send = [];
    let topic_str = subTopic(req.body.topic)
    console.log(topic_str)
    let rows = await db('tbl_electric_match').select('topic', 'command').where('topic', 'like', topic_str)
    for (let i = 0; i < rows.length; i++) {
        send.push({ topic: rows[i].topic + 'STATUS', command: rows[i].command })
    }
    console.log(send)
    res.send(send)


    /* var options = {
            clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
            username: "nelleh",
            password: "qazwsx",
            clean: true
        }
        //console.log(db)
    var client = mqtt.connect('mqtt://staging.nellehliving.com', options)
    client.on('connect', function() {
            client.subscribe(req.body.topic, function(error) {
                if (!error) {

                    //res.send({ message: 'success' })
                } else {
                    //res.send({ message: 'fail' })
                }
            })
        }) */

    //await client.subscribe(req.body.topic)

    /* client.on('message', async function(topic, message) {
        console.log({ topic: topic, message: message.toString() })
            //await db('tbl_electric_match').where('topic', '=', topic.split('STATUS').join('')).update({ command: message.toString() })
        client.end()
    }) */

})

function subTopic(topic) {
    let str = ''
    if (topic.indexOf('+') > 0) {
        str = '+';
    } else if (topic.indexOf('#') > 0) {
        str = '#'
    }
    let topic_check = topic.split('STATUS').join('')

    let topic_sub = (str == '') ? topic_check : topic_check.split(str).join('%')

    return topic_sub;
}