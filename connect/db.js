const config = require('../config/config_db');

const knex = require('knex')({
    client: 'mysql',
    connection: config.db_multi,
    debug: true,
})

module.exports = knex;