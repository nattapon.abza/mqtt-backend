const nodemailer = require('nodemailer');

// async..await is not allowed in global scope, must use a wrapper
var sendmail = async function main(to, username, password) {
    let message = 'System details<br>' +
        'Username : ' + username + '<br>' +
        'Password : ' + password.toString() + '<br>' +
        'Please change your password after first using.'
    let account = {
        email: 'info.nellehliving@gmail.com',
        pass: 'wratpassword'
    }

    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        auth: {
            user: account.email,
            pass: account.pass
        }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"Nellehliving" <info.nellehliving@gmail.com>', // sender address
        to: to,
        subject: 'You have to register successfully.', // Subject line
        html: message // html body
    });

    console.log('Message sent: %s', info.messageId);

    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

}

module.exports = sendmail